If you want to use PlayMaker :

- Close Unity
- Open the folder ".../Assets/LoginPro/Use my content/PlayMaker"
- "Extract here" the archive called [PlayMaker.zip]
- Open Unity, Insert all the scenes of the folder "Scenes" (you've just extracted) in the build settings (Edit -> Build Settings...)

That's all :)
