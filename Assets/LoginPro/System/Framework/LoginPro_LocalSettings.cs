
                    public static class LoginPro_LocalSettings
                    {
                        // The URL to reach the connection script named 'Server.php'
                        public static string URLtoServer = "www.chillwizardgames.com/LoginPro_Server/Game/Server.php";

                        // The game name
                        public static string GameName = "ImagineThat!";

                        // The version of the game, the server will compare this number with his own and alert the client if a new version is available
                        public static string GameVersion = "1.0";
                    }
                    