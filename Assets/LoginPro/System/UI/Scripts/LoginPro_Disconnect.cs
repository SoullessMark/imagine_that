﻿using UnityEngine;
using System.Collections;

public class LoginPro_Disconnect : MonoBehaviour
{
    public UIAnimation AnimationHideCurrentWindow;
    public UIAnimation_Alert AnimationShowMessage;
    public UIAnimation AnimationShowLogin;

    public void Launch()
    {
        // Clear the session
        LoginPro.Session.ClearSession();

        // Hide menu and show login
        // Launch all animations one after the other
        StartCoroutine(LaunchRegisterAnimations());
    }

    private IEnumerator LaunchRegisterAnimations()
    {
        // Hide current window
        if (this.AnimationHideCurrentWindow != null)
            yield return this.AnimationHideCurrentWindow.Launch();

        // Show message on success
        if (this.AnimationShowMessage != null)
            yield return this.AnimationShowMessage.Show("You are disconnected.", 2);

        // Launch animation on success
        if (this.AnimationShowLogin != null)
            yield return this.AnimationShowLogin.Launch();

        yield return null;
    }
}
