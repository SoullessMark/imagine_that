﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorChangerScript : MonoBehaviour {

    public List<Text> ListTitle = new List<Text>();
    public Image BG;
    private Color color;
    private int count = 0;
    private float current = 0;
    private int i;

	void Start () {
        count = ListTitle.Count;
    }

    void Update() {

        color = Color.HSVToRGB(current, .3f, .9f);

        for (i = 0; i < count; i++)
        {
            ListTitle[i].color = color;
        }

        if (BG != null) { 
            BG.color = color;
        }
        current += .0022f;

        if ( current >= 1)
        {
            current = 0;
        }
    }
}
