﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UTIL_LERP : MonoBehaviour {
    private static UTIL_LERP _GET;
    public static UTIL_LERP GET
   
    {
        get
        {
            if(_GET == null)
            {
                GameObject go = new GameObject("UTIL_LERP");
                go.AddComponent<UTIL_LERP>();

            }
            
                return _GET;
            
        }
    }

    void Awake()
    {
        _GET = this;
    }
    public void LERP(GameObject initial, Vector3 target, float snap = .35f)
    {
        StartCoroutine(_LERP(initial, target,snap));
    }
    IEnumerator _LERP(GameObject init, Vector3 tar, float snap)
    {
        float DISTANCE = Mathf.Sqrt(Mathf.Pow((tar.x - init.transform.localPosition.x), 2) + Mathf.Pow((tar.y - init.transform.localPosition.y), 2));

        while (DISTANCE > .01f)
        {
            init.transform.localPosition = Vector3.Lerp(init.transform.localPosition, tar, Time.deltaTime * 3.5f);
            DISTANCE = Mathf.Sqrt(Mathf.Pow((tar.x - init.transform.localPosition.x), 2) + Mathf.Pow((tar.y - init.transform.localPosition.y), 2));
            
            if (DISTANCE > snap)
            {
                
                yield return new WaitForSeconds(.01f);
            }
            else
            {
                init.transform.localPosition = tar;
                yield return null;
            }
        }
        

    }
}
