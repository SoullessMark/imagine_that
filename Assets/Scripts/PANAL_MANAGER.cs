﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PANAL_MANAGER : MonoBehaviour {
    public GameObject[] PANELS;
    public int ACTIVE_PANEL;
    public int LAST_ACTIVE_PANEL;
    private Vector3 TARGET;
    
    void Start()
    {
        LAST_ACTIVE_PANEL = 0;
    }

    public void TRANSITION_CHECK()
    {
        if(LAST_ACTIVE_PANEL != ACTIVE_PANEL)
        {
            SET_TARGET();
            UTIL_LERP.GET.LERP(gameObject, TARGET);
        }
    }

    public void ON_PLAY_CLICK()
    {
        ACTIVE_PANEL = 1;
    }
    public void ON_CARDS_CLICK()
    {
        ACTIVE_PANEL = 2;
    }
    public void ON_SHOP_CLICK()
    {
        ACTIVE_PANEL = 3;
    }
    public void ON_OPTIONS_CLICK()
    {
        ACTIVE_PANEL = 4;
    }

    
    public void SET_TARGET()
    {
        Vector3 POS = PANELS[ACTIVE_PANEL].transform.localPosition;
        TARGET = new Vector3(POS.x * -1f, POS.y * -1f, POS.z);
       
    }
}
