﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CARD_DISPLAYER : MonoBehaviour {
    [SerializeField]
    private int USER_CARDS;
    private int CURRENT_USER_CARD = 0;
    public GameObject BLANK_CARD;
    public GameObject[] BLANK_CARDS;
    public GameObject COLUMN;
    public GameObject[] BLANK_CARD_COLUMNS;
	// Use this for initialization
	void Start () {
        COLUMN = new GameObject("BLANK_COLUMN");
        BLANK_CARD_COLUMNS = new GameObject[5];
        BLANK_CARDS = new GameObject[15];
        CURRENT_USER_CARD = 0;
        for (int i = 0; i < 5; i++)
        {
            BLANK_CARD_COLUMNS[i] = Instantiate(COLUMN, gameObject.transform.position,gameObject.transform.rotation);
            if(i > 0)
            {

            }
            else
            {

            }
            BLANK_CARD_COLUMNS[i].transform.parent = gameObject.transform;
            for(int k = 0; k < 3; k++)
            {
                BLANK_CARDS[CURRENT_USER_CARD] = Instantiate(BLANK_CARD, gameObject.transform.position, gameObject.transform.rotation);
                BLANK_CARDS[CURRENT_USER_CARD].transform.parent = BLANK_CARD_COLUMNS[i].transform;
                CURRENT_USER_CARD++; 
            }
        }
	}

    void SNAP_CARD()
    {

    }
    void SNAP_COLUMN()
    {

    }
	
}
